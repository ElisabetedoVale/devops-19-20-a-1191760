# Technical Report:  Build Tools with Gradle

### Class Assignment 2 (CA2) - Part 2

This technical report was developed in context of **DEVOPS** discipline belonging to the **2019-2020 SWitCH Program** from **I.S.E.P.**.

#### Part I - Project creation: sources and repository

The goal of the present assignment is to convert the basic version (i.e., "basic" folder) of the CA1 assignment to an application Gradle (instead of Maven).
The download of the basic project for this assignment was done from [CA1/tut-basic/](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA1/tut-basic/).
All the developed work has been saved on folder [CA2_Part2](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part2).
The commands lines proposed presuppose its execution on the console *Windows PowerShell*.

##### Step 1: Create an independent line of development: new branch "tut-basic-gradle"

To create a new branch and starting to work on it, we just need to execute the following command:

```console
$ git checkout -B tut-basic-gradle
```

#### Part II - Start a gradle spring project

To start a new gradle spring project we can use spring initializr on https://start.spring.io.
Already on **spring initializr**, we select the option "**gradle project**" and the following dependencies: **Rest Repositories**; **Thymeleaf**; **JPA** and **H2**.
As result of this step we will have a spring boot application that we can build with gradle.

#### Part III - Add the "empty" spring application to repository

##### Step 1: Add spring application to "CA2_Part2" folder

The zip file resulted of spring initializr, must be unziped and added to the "CA2_Part2" folder.

##### Step 2: Check the available gradle tasks

With the terminal opened on **CA2_Part2\demo** folder we can check the gradle tasks executing: 
```console
$ ./gradlew tasks
```
This command show to us some tasks that available in the project such as: boot run (to run the project as a Spring Boot application); specific java tasks; generic gradle tasks; tasks to generate java.doc and others.

#### Part IV - Remove the src 'default' folder of spring application

The spring project already has a src folder that has code but it doesn't interest us so we start by deleting this folder, using the command:
```console
$ git rm -r src
```
Follow by:
```console
$ git commit -a -m "close #14: Remove src folder of spring application"
$ git push origin
```
#### Part V - Add files from basic tutorial

##### Step 1: Copy the src folder of CA1 exercise to spring application

Using the windows of Microsoft we can copy the the **SRC folder** of **CA1** exercise to our spring project. With the src folder the must copy the files: **webpack.config.js** and **package.json** that will configure the frontend of application.
To add the files to our remote repository we execute:
```console
$ git add --all
$ git commit -a -m "close #15, #16: Add some files of CA1 to spring application"
$ git push origin
```
##### Step 2: Delete the folder src/main/resources/static/built/

##### Step 1: Delete folder 'built'

The folder '**built**' have some javascript files that will be generated ond compilation process, so this folder should be deleted.

```console
$ git commit -a -m "close #17: Delete folder src/main/resources/static/built/"
$ git push origin
```

#### Part VI -Experiment the application

##### Step 1: Execute spring application

Now is time to execute the spring application , executing (on demo folder) the follow command line:

```console
$ ./gradlew bootRun
```

##### Step 2: Check localhost

With the spring boot running we must check our localhost on http://localhost:8080, and certify that it is working but is empty.
At this moment the spring server it's working but the frontend it's not, this it happening because is missing the gradle plugin for dealing with the frontend code.

#### Part VII - The gradle plugin org.siouan.frontend

To the application gradle also be able to manage the frontend we need to add the gradle plugin org.siouan.frontend. This plugin can be consulted on https://github.com/Siouan/frontend-gradle-plugin and its similar to the one used in the Maven Project (frontend-maven-plugin).
The maven plugin used can be consulted on https://github.com/eirslett/frontend-maven-plugin.

#### Part VIII - Add the new gradle plugin org.siouan.frontend
  
##### Step 1: Add new plugin
  
To add the plugin gradle we must add the following script to the **plugins** block on **build.gradle** file:
  
[File: build.gradle](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part2/demo/build.gradle)
  
```
plugins {
	id "org.siouan.frontend" version "1.4.1"
}
```

#### Part IX - Configure new plugin

The plugin is added but know the need to add them some configurations.

[File: build.gradle](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part2/demo/build.gradle)
```
frontend {
nodeVersion = "12.13.1"
assembleScript = "run webpack"
}
```
These settings indicate the node version as well the assembleScript task.
The assembleScript invokes the webpack tool which allows to transform the react javascript in order to be served by the web server.

#### Part X - Update the scripts section/object in package.json

The execution of webpack tool implies actualization of the package.json scripts block, so we need to add the webpack script:
  
```
"scripts": {
"watch": "webpack --watch -d",
"webpack": "webpack"
}
```
#### Part XI - Build the gradle

With the plugin of fronted we need now to build the gradle, executing the following command line:

```console
$ ./gradlew build
```

After building we can verify that the build gradle generate for itself the folder 'built' and javascript files that have been deleted before.

#### Part XII - Run the application

##### Step 1: Execute spring application
The gradle have been build successfully and now we must execute the application, executing the command line:

```console
$ ./gradlew bootRun
```

##### Step 2: Check localhost

With the springboot running we must check our localhost at http://localhost:8080, and certify that server and frontend its working successfully.

#### Part XIII - Add a new task to copy the generated jar to a folder

To create a new gradle tool able to copy the generated jar to a new folder we must add on the build.gradle file the following script:

[File: build.gradle](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part2/demo/build.gradle)

```
task backupGeneratedJar(type: Copy) {
    from '/build/libs'
    include "demo-0.0.1-SNAPSHOT.jar"
    into 'dist'
}
```

This folder named "dist" will be located a the project root folder level.

#### Part XIV - Add a new task to delete all the files generated by webpack

To carry out this task we must add on the **build.gradle** file the following script:

[File: build.gradle](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part2/demo/build.gradle)

```
task deleteFilesWebpack(type: Delete) {
    dependsOn clean
    delete '/src/main/resources/static/built'
}
```

#### Part XV - Experiment the developed features

The new tasks are added to gradle and now they must be tested.

##### Step 1: To test the first task we must execute:

```console
$ ./gradlew backupGeneratedJar
```

And check that have been created the **dist** folder with the jar file.

##### Step 2: For the second task we must execute:

```console
$ ./gradlew deleteFilesWebpack
```

And we can verify that javascript files from folder 'built' have been deleted.

#### Part XVI - Alternative to Gradle: **Bazel** build tool

As a gradle alternative we have for example Bazel, both are an open-source build and tests toll.
  
As well as Gradle, Bazel uses a high-level build language and have a similar level of functionality.
  
Gradle, offers a flexible, stateful, object-oriented interface.
  
Bazel provides a structured system that is easy to reason about and provides a strong, functional foundation for large and growing products.
  
Therefore Bazel are made to supports projects in multiple languages, an large codebases across multiple repositories, and a large numbers of users, added to the fact that it builds outputs for multiple platforms.
  
Additionally Bazel is used to build the big part of Google's software, so it has been designed to handle build problems of Google's development environment.
  
The download of this build tool could be done at https://docs.bazel.build/versions/2.2.0/install.html.


#### Part XVII - Tag the end of assignment
  
##### Step 1: Create a new tag on master "CA2-part2"

The CA2 - Part2 assignment is completed and so a tag should be created to mark the end of it.
To create the Tag locally, we execute:

```console
$ git tag -a CA2-part2 -m "CA2-part2 exercise it's complete"
```

##### Step 2: Add the new tag to origin
The new tag have been locally created and must sent to the remote repository using the command:

```console
$ git push origin CA2-part2
```

 This tag mark the end of the assignment.