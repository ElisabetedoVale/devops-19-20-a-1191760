# CI/CD Pipelines with Jenkins
  
   
### Class Assignment 5 - Part 1
  
  
This technical report was developed in context of *DevOps* discipline, belonging to the *2019-2020 SWitCH Program* from *I.S.E.P.*.
    
The goal of the present assignment is to practice with Jenkins using the "gradle-basic-demo" project.
  
   
## Something about Jenkins
  
Jenkins is a free and open source automation tool to Continuous Integration (CI) and Continuous Delivery (CD) process.
  
Written in Java programming language, it works like a server capable of orchestrating a chain of actions that help developers on the CI/CD workflows, called by pipelines.
  
![](Images/CI_CD.PNG)
  
The idea of CI is to merge code from individual developers into a project multiple times per day and test continuously to avoid downstream problems.
  
The CD takes this a step further to ensure that all merged code is always in a production-ready state.
 
Currently, Jenkins have more than 1500 community contributed plugins to support building, deploying, and automating of any project.
  
![](Images/Jenkins_Plugins.PNG)
  
  
This automation makes it easier to integrate project changes, and easier to obtain a fresh build, which increases the developers productivity.
 
#
  
**Jenkins Advantages**

  - Free of cost.
  - Easy to install and does not require additional installations or components.
  - Easily modified and extended according to the requirements for continuous integrations and continuous delivery.
  - Platform Independent. Available for all platforms and different operating systems, whether OS X, Windows or Linux.
  - Rich Plugin ecosystem. The extensive pool of plugins offers more flexibility.
  - Issues are detected and resolved almost right away.
  - Can favor less integration problems.  If we choose to automate the integration of work.
 
#
     
**What Jenkins can do for us?**

Jenkins have many plugins that can help us to automate a lot of processes, some of them are:

  - Automated testing
  - Distributed Builds/Testing
  - Code Analysis
  - Metrics & Trends
  - Reporting
  - Automated Deployment
  
## 1 | Creation of a simple Pipeline
 
#
  
**CREATE A WORK DIRECTORY**
   
Firstly, we must create a new directory to do our work, so we execute:
 ```
 $ mkdir CA5-part1
 ```
  
To change our path to the new folder we use:
 ```
 $ cd CA5-part1
 ```

This assignment will also need a readme file to document the work done.
To create that we execute:
 ```
 $ echo README >> README.md
 ```
  
After creating the issue on *Bitbucket*, we must commit all the changes executing:
  
 ```
 $ git add --all
 ```

 ```
 $ git commit -m "close #46: Initial commit: CA5_Part1"
 ```

 ```
 $ git push
 ```
 
#
  
**INSTALL JENKINS**
   
There are many ways to install Jenkins as we can consult at https://jenkins.io/download/.

One of these ways and taking into account that Jenkins is a java application, is to **running directly a war file**.
To this assignment we will choose that option. To conclude them we must:

  - Follow to the website https://jenkins.io/doc/book/installing/
    
  - Search for the WAR file option, read and follow the instructions on that topic, such as:
  
    - 1st: **Download** the *latest stable Jenkins WAR file*.
    - 2nd: Change the location of **jenkins.war** file to the directory chosen to run it.
	- 3rd: On console, to **run the jar command**, execute:
    
		 ```
		 $ java -jar jenkins.war --httpPort=9090
		 ```
  
*Note 1:* In order to avoid possible conflicts, we choose run Jenkins at the port 9090 as an alternative to the port 8080.
*Note 2:* Jenkins should still running during all the next processes.
    
  - Now we must **Open** the localhost at the previously defined port: http://localhost:9090
	
  - To **unlock Jenkins** use the password automatically generated on *PowerShell*
	  
    ![](Images/Jenkins_Password.PNG)
	
  - Already on **Jenkins page** we should:
	
	- 1st: **Customize Jenkins**: Install suggested plugins.
	- 2nd: **Create first admin user**: Define Username; Password; Full Name and Email.
	- 3rd: **Instance configuration**: Define Jenkins URL.
	  
 
#
  
**CREATE A NEW JOB**

The Job concept is applied to a new process/project that will be performed.
  
Each job consists of stages that are, in the end, logical groupings of steps.
  
That steps correspond to new tasks that are performed on one or more Nodes.  
In the absence of any indication (additional settings) the Node assumed by Jenkins is the computer where Jenkins is running.

To create a new job, at **Jenkins page**, we must:

  - Select **New Item**
  - Define an **item name**: *devops-gradle-basic-demo*
  - Choose the **Pipeline** option
  
Now we can define some settings like add a Description or define Build Triggers, but we will just focus on the Pipeline options.
    
Already on the **Pipeline guide**, we must:
  
  - Select the **Pipeline Definition** as *Pipeline script from SCM*
  
	If the intention were to write the text of the pipeline, we would choose the option Pipeline Script.
	As we intend to use a Jenkinsfile where we will define all the Job Stages, we must choose the Script from SCM.
	Note that: The Jenkinsfile must have on the root of the project: CA2_Part1/gradle_basic_demo directory.
  
  - Select the **SCM** as *Git*
    As we intent to use GIT to access at the repository.
    
  - Define the **Repository URL**: https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760
  
*Note that:* Any credentials will be required, since this repository is not private, otherwise we would have to be defined.
 
  - Define the **Script Path** to the directory of Jenkinsfile: CA2_Part1/gradle_basic_demo/Jenkinsfile
    
## 2 | Definition of Pipeline Stages 

#
  
**CREATE A JENKINSFILE**

The pipeline is already created and now, on the root directory of project *CA2_Part1\gradle_basic_demo*, we create a Jenkinsfile, executing:

 ```
 $ echo JENKINSFILE >> Jenkinsfile
 ```
 
#
  
**CREATE THE STAGES OF PIPELINE**

The Jenkinsfile is create and now we should edit them 
 
  * CHECKOUT - To check out the code forms the repository.
  * ASSEMBLE - To compiles and Produces the archive files with the application. Do not use the build task of gradle.
  * TEST - Executes the Unit Tests and publish in Jenkins the Test results.
  * ARCHIVE - Archives in Jenkins the archive files (generated during Assemble).
 
#
    
[Jenkinsfile](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part1/gradle_basic_demo/Jenkinsfile)
  
```
  pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://ElisabetedoVale@bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760.git'
            }
        }

        stage('Assemble') {
            steps {
               dir("CA2_Part1/gradle_basic_demo"){
                   echo 'Assemble...'
                   bat 'gradlew clean build -x test'
               }
            }
        }
        stage('Test') {
             steps {
                dir("CA2_Part1/gradle_basic_demo"){
                    echo 'Test...'
                    bat 'gradlew test'
                 }
              }
         }
        stage('Archive') {
            steps {
                dir("CA2_Part1/gradle_basic_demo"){
                echo 'Archive...'
                archiveArtifacts 'build/distributions/*'
                }
              }
            }

   } /* Stages*/

   post {
      always {
        junit '**/test-results/test/*.xml'
      }
   }

  } /* pipeline */
 
```
 
#
   
**ADD JENKINSFILE TO REPOSITORY**
 
 ```
 $ git add Jenkinsfile
 ```
 
 ```
 $ git commit -m "ref #47: ADD Jenkinsfile"
 ```
 
 ```
 $ git push
 ```
 
#
   
**BUILD THE PIPELINE**
   
The pipeline and all stages are done so we can build it on Jenkins localhost: http://localhost:9090.
 
#
  
We must select the option **Build Now** and if build was successfully all the stages will appear green on the **Stage View**, like the following image shows.
 
#
   
![](Images/Jenkins_successfully_build.PNG)
 
#
  
The **build history** gives us navigability for each build started.
  
If we click on the build and it still running, we can see, on **Console Output**, the whole process that is being executed by stages.
  
If the build has already done, the history of processes executed on Console is still accessible for consult.
 
#
    
![](Images/Jenkins_Console_Output.PNG)
 
#
  
In the same line as Console Output, we can check the **Workspace**. There, is possible to browse between directories and assure that if the build as done as expected.
 
#
    
![](Images/Jenkins_Workspace.PNG)
 
#
  
In the same line as Workspace, we can check the **Test Result**.
 
#
  
![](Images/Jenkins_Test_Result.PNG)
 
#
  
To have a UI more user friendly we can install the **Blue Ocean Plugin** and browse on Jenkins.
 
#
  
![](Images/Jenkins_Blue_Ocean.png)
 
#  
  
![](Images/Jenkins_Blue_Ocean2.PNG)
 
#
   
## 3 | End of assignment part Tag
  
  - Create a new tag on master "CA5-part1"
  
This part of assignment is completed and so a tag should be created to mark the end of it.
To create the Tag locally, we execute:
  
 ```
 $ git tag -a CA5-part1 -m "CA5-part1 exercise it's complete"
 ```
  
  - Add the new tag to origin
The new tag has been locally created and must sent to the remote repository using the command:
  
 ```
 $ git push origin CA5-part1.
 ```
  
This tag marks the end of the Part 1 of assignment.

  
  
  