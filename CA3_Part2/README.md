# Practice Virtualization with vagrant


### Class Assignment 3 - Part 2

This technical report was developed in context of *DevOps* discipline, belonging to the *2019-2020 SWitCH Program* from *I.S.E.P.*.

#
## PART 0 - Project information - Sources and Repository
#

The goal of the present assignment is to practice with VirtualBox using the same projects from the previous assignments, but now inside a Virtual Machine with Ubuntu. 

The basic project for this assignment results from:

  - A Vagrant initial solution, whose download can be done at: [vagrant-multi-spring-tut-demo](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/);
  
  - An individual project of CA2_Part2, whose clone can be done at: [devops-19-20-a-1191760](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760).

The technical documentation that describe the process has been saved on folder [CA3_Part2](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2).

#
## PART I - Download of Vagrantfile
#

Vagrant allows us to streamline the process of creating Virtual Machines (VMs) helping us to automate the setup of one or more VMs.
It happens because Vagrant allows to define all of the VM configuration, using a simple configuration file (stardartically called by Vagrantfile).
Therefore become it so easy to reproduce environments, and we can even say that sharing a VM becomes sharing a Vagrantfile.

For this assignment we will use a previous Vagrantfile as initial solution, and it should be adapted to our project.
The download of this initial solution can be done at https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/.

#
## PART II - Analysis of Vagrantfile
#

Watching the downloaded folder, we can verify that, with the Vagrantfile, comes a readme file that describe the steps to put the Vagrant scenario on.
Checking this readme file we get the follow information:

  - Vagrantfile will create two vagrant VMs (Web and DB)
  
	- Web VM executes the web application (Spring Boot) inside of Tomcat8
	
	- DB VM executes the H2 database as a server process.

  - The Vagrantfile execution requires a previous installation of Vagrant and VirtualBox in our computer;
  
  - Description of the steps that we must follow to install Vagrantfile.

#
## PART III - Copy of Vagrantfile to repository
#

#### Step 1 |  Create a new directory

First, we need to have a directory to start to work.
Therefore, already on our local repository folder, we create a new directory and move to that, executing:

 ```
 $ mkdir CA3-Part2
 $ cd CA3-Part2 
 ```

#
#### Step 2 |   Download of Vagrantfile

Through the windows environment we copy the Vagrantfile previous downloaded to CA3-Part2 on our local repository.
To send the file and folder of assignment to our remote repository we execute:

 ```
 $ git add --all
 $ git commit -m "close #25: Add Vagrantfile initial solution - CA3-Part2 assignment"
 $ git push
 ```

#
## PART III - Replication of Vagrantfile Scenario
#

The Vagrantfile initial solution is already configured to crated two Vms: one Web Vm and one Database VM.
The objective of this assignment is to change Vagrantfile configuration so that the VMs created take on our Spring Boot Project Application.

To make that possible we need to change the provision configurations of [Vagrantfile](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/Vagrantfile):

  - 1st. change the git clone path to our remote repository:
  
     *git clone https://ElisabetedoVale:wwuahp3PX9@bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760.git*

    
  - 2nd: change to Spring Boot location
  
     *cd devops-19-20-a-1191760/CA3_Part2/gradle_Spring*


  - 3rd: make gradlew executable
  
     *./gradlew clean build*

After Vagrantfile configuration we must commit the changes, executing:

 ```
 $ git add Vagrantfile
 $ git commit -m "ref #27: UPDATE Vagrantfile to use my gradle spring app"
 $ git push
 ```

#
## PART IV - Adaptation of Spring Boot Application
#

With the Vagrantfile already configured we need now to adapt the Spring Boot Application to use the H2 server in the db VM.

#
### Step 1 |   Download the Spring Boot project


First e need to clone the Spring boot project *'gradle_basic_demo'* so we execute the following command line:
 
 ```
 $  git clone https://ElisabetedoVale@bitbucket.org/ElisabetedoVale/simplechatapplication.git
 ```

#
### Step 2 |   Delete unnecessary folders from project


Some files of Spring Boot project may generate conflicts in the construction of our VMs.
To avoid these conflicts we should delete those files, leaving only the "gradle" and "src" directories and all the files that are out of directories.

To remove the the  unnecessary folders we execute:

 ```
 $  rm -r node
 $  rm -r node_modules
 $  rm -r dist
 $  rm -r build
 $  rm -r .gradle
 ```


### Step 3 |   Commit and change the Spring Boot project


#### To configure our project for Vagrant execution we must follow the next **8 STAGES**:


**(1) Initial commit - Spring Boot initial project**

First of all we need to commit our spring boot project from our local repository to the remote repository.
We must execute the following command:

 ```
 $ git add --all
 $ git commit -m "close #28: ADD initial Project Spring Boot Application (CA2_Part2)"
 ```

 
**(2) ADD Support for building war file**


[build.gradle](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/gradle_Spring/build.gradle)


```java
	id 'org.siouan.frontend' version '1.4.1'
	id 'war'
    
```
  
```java
 	testImplementation('org.springframework.boot:spring-boot-starter-test') {
 		exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
 	}
	// To support war file for deploying to tomcat
	providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
    
```

[src/main/java/com/greglturnquist/payroll/ServletInitializer.java]("https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/gradle_Spring/src/main/java/com/greglturnquist/payroll/ServletInitializer.java")

```java
    package com.greglturnquist.payroll;

    import org.springframework.boot.builder.SpringApplicationBuilder;
    import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

    public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }
    }
    
```

To add the changes to our remote repository, we execute:

```
$ git add --all
$ git commit -m "ref# 29, close #30 ADD Support for building war file"
```

**(3) ADD Support for h2 console**


[src/main/resources/application.properties]("https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/gradle_Spring/src/main/resources/application.properties")

```java
 spring.data.rest.base-path=/api
 spring.datasource.url=jdbc:h2:mem:jpadb
 spring.datasource.driverClassName=org.h2.Driver
 spring.datasource.username=sa
 spring.datasource.password=
 spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
 spring.h2.console.enabled=true
 spring.h2.console.path=/h2-console
 
```

To add the changes to our remote repository, we execute:

```
$ git add src/main/resources/application.properties
$ git commit -m "ref #29, close #31 ADD Support for h2 console"
```

**(4) ADD weballowothers to h2**


[src/main/resources/application.properties]("https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/gradle_Spring/src/main/resources/application.properties")

```java
spring.h2.console.settings.web-allow-others=true

```

To add the changes to our remote repository, we execute:

```
$ git add src/main/resources/application.properties
$ git commit -m "ref#29, close#32: ADD weballowothers to h2"
```
 
**(5) ADD: Application context path**


[src/main/js/app.js]("https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/gradle_Spring/src/main/js/app.js")

```
 client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {
 
```

[src/main/resources/application.properties]("https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/gradle_Spring/src/main/resources/application.properties")

```
 server.servlet.context-path=/demo-0.0.1-SNAPSHOT
 
```

To add the changes to our remote repository, we execute:

```
$ git add --all
$ git commit -m "ref#29, close#33: ADD Application context path"
```


**(6) UPDATE: Fixes ref to css in index.html**


[src/main/resources/templates/index.html]("https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/gradle_Spring/src/main/resources/templates/index.html")

```
    <link rel="stylesheet" href="main.css" />
    
```

To add the changes to our remote repository, we execute:

```
$ git add src/main/resources/templates/index.html
$ git commit -m "ref#29, close#34: UPDATE: Fixes ref to css in index.html"
```


**(7) ADD: Settings for remote h2 database**


[src/main/resources/application.properties]("https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/gradle_Spring/src/main/resources/application.properties")

```java
 #spring.datasource.url=jdbc:h2:mem:jpadb
 spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
 
```

To add the changes to our remote repository, we execute:

```
$ git add src/main/resources/application.properties
$ git commit -m "ref#29, close#35: ADD Settings for remote h2 database"
```


 > **(8) UPDATE: So that spring will no drop de database on every execution**


[src/main/resources/application.properties]("https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2/gradle_Spring/src/main/resources/application.properties")

```java

 # So that spring will no drop de database on every execution.
 spring.jpa.hibernate.ddl-auto=update
 
```

To add the changes to our remote repository, we execute:

```
$ git add src/main/resources/application.properties
```
  
```
$ git add src/main/resources/application.properties
$ git commit -m "ref #29, close #36 ADD property to spring no drop de database on every execution"
```

All the changes are done and committed but we need do push them to the remote repository, executing:

 ```
 $ git push
 ```

#
### Step 4 |   Execute the Vagrantfile

Already with Vagrantfile in our compute we must execute:

 ```
 $ vagrant up
 ```

The two vagrant VMs will be generated according to Vagrantfile configurations.
The first time that Vagrant is executed it will take a little longer because both VMs will be provisioned with several software packages.

#
### Step 5 |   Test Spring Boot Application

  - To check if the **spring web application** is running correctly we can use the following options:

http://localhost:8080/demo-0.0.1-SNAPSHOT/
*or*
http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/

And we can see that web VM is running correctly:

![](./Images/SpringBoot_webVM.PNG)


  - To test the **spring db application** we can also follow:

http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
*or*
http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console

For the connection string use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb

And we can see that db VM is running correctly:


![](./Images/SpringBoot_dbVM.PNG)


#
## PART V - Readme file
#

The assignment is complete and to document the work done. 
The folder of assignment is created, and the next step is creating the readme file, executing:

 ```
 $ echo README >> README.md
 ```

After describing all the processes of the assignment on the README file, and after create issue on *Bitbucket*, we must commit all the changes executing the following commands:

```
$ git add readme.md
$ git commit -m "close #40: ADD Readme File"
$ git push
```

#
## PART VII - End of assignment Tag
#

  - Create a new tag on master "CA3-part2"

The CA3 - Part2 assignment is completed and so a tag should be created to mark the end of it.
To create the Tag locally, we execute:

 ```
 $ git tag -a CA3-part2 -m "CA3-part2 exercise it's complete"
 ```

  - Add the new tag to origin
The new tag has been locally created and must sent to the remote repository using the command:

 ```
 $ git push origin CA3-part2.
 ```
  
This tag marks the end of the part 2 of assignment.