# Vagrant Multi VM Demonstration for Spring Basic Tutorial Application

This project demonstrates how to setup two VMs for running the Spring Basic Tutorial application.

**web** VM:

  - Executes the web application inside Tomcat8

**db** VM:

  - Executes the H2 database as a server process. The web application connetcs to this VM.

This vagrant setup uses the spring appplication available at https://bitbucket.org/atb/tut-basic-gradle.

**Important** You should replace this version of the spring applicationn with your own. Do not forget to replicate the necessary changes in our own version! These regard essentially the support for using H2 in server mode (as opposed to memory mode).

## Requirements

  Install Vagrant and VirtualBox in you computer.

## Steps

### 1

Create a local folder in your computer and copy the Vagrantfile into that folder

### 2

Execute **vagrant up**

This will create two vagrant VMs (i.e, "db" and "web"). The first run will take some time because both machines will be provisioned with several software packages.

### 3

In the host you can open the spring web application using one of the following options:

  - http://localhost:8080/basic-0.0.1-SNAPSHOT/
  - http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/

You can also open the H2 console using one of the following urls:

- http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
- http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console

For the connection string use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
