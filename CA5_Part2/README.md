# CI/CD Pipelines with Jenkins
  
   
### Class Assignment 5 - Part 2
  
This technical report was developed in context of *DevOps* discipline, belonging to the *2019-2020 SWitCH Program* from *I.S.E.P.*.
    
The goal of the present assignment is to is to create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2).
   
**PREVIOUS REQUIREMENTS:** The part 2 of this assignment presuppose that part 1 are already done and we already have Jenkins on our computer.
  
## 1 | Pipeline Creation
#

**CREATE A WORK DIRECTORY**
   
Firstly, we must create a new directory to do our work, so we execute:
  
 ```
 $ mkdir CA5-part2
 ```
  
To change our path to the new folder we use:
  
 ```
 $ cd CA5-part2
 ```

This assignment will also need a readme file to document the work done.
To create that we execute:
  
 ```
 $ echo README >> README.md
 ```
  
After creating the issue on *Bitbucket*, we must commit all the changes executing:
  
 ```
 $ git add --all
 ```

 ```
 $ git commit -m "close #49: Initial commit: CA5_Part2"
 ```

 ```
 $ git push
 ```
 
#
  
**RUN THE JENKINS.WAR FILE**
   
  - On console, **run the jar command** to start Jenkins:
		 ```
		 $ java -jar jenkins.war --httpPort=9090
		 ```
  
*Note 1:* We are choosing to run Jenkins at the port 9090 as an alternative to the port 8080.
  
*Note 2:* **Jenkins should still running during all the next processes**.  
  
    
  - On browser, **Open** the Jenkins page at the localhost previously defined port: http://localhost:9090

#

**CREATE A NEW JOB**
  
The Job concept is applied to new process/project so, in order to that, a new one should be created.
  
To create a new job, at **Jenkins page**, we must:

  - Select **New Item**
  - Define an **item name**: *devops-1191760-gradle-basic-demo_part2*
  - Choose the **Pipeline** option
  
Now we define the Pipeline settings, on the **Pipeline guide**:
  
  - Select the *Pipeline Definition* as **Pipeline script from SCM**
	As we intend to use a Jenkinsfile where we will define all the Job Stages.
  
  - Select the *SCM* as **Git**
    As we intent to use GIT to access at the repository.
    
  - Define the **Repository URL**: *https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760*
  
*Note 3:* Any credentials will be required, since this repository is not private, otherwise we would have to be defined.
 
  - Define the **Script Path** to the directory of Jenkinsfile: *CA2_Part2/demo/Jenkinsfile*
    
## 2 | Pipeline Stages 
#
  
**CREATE A JENKINSFILE**
  
To create a Jenkinsfile, on the root directory of project *CA2_Part2\demo*, we execute:
  
 ```
 $ echo JENKINSFILE >> Jenkinsfile
 ```
  
#
  
**CREATE THE STAGES**
  
The Jenkinsfile is created and now we should add to it the required stages.
    
Some of that stages already have been defined on the Jenkisfile of CA5-Part1.
  
*Required stages:*
  
  * **CHECKOUT** - To check out the code from the repository.
    - This stage has not suffered any changes.
    
  * **ASSEMBLE** - To compiles and Produces the archive files with the application, but without running the tests.
    - Updated the working directory to this stage.
    
  * **TEST** - Executes the Unit Tests and publish in Jenkins the Test results.
    - Updated the working directory to this stage.
      
  * **JAVADOC** - Generates the javadoc of the project and publish it in Jenkins.
  
    - New stage added with a step to:
    
      - execute the Javadoc, generating de Javadoc files;
      
      - archive/publish the javadoc html reports (this Pipeline script have been writing using the Jenkins tool Pipeline Syntax);
  
```
   stage('Javadoc') {
       steps {
           dir("CA2_Part2/demo"){
           echo 'Generating Javadoc...'
            bat 'gradlew javadoc'
            publishHTML([allowMissing: false,
            alwaysLinkToLastBuild: true, 
            keepAll: true, 
            reportDir: '', 
            reportFiles: 'reportindex.html', 
            reportName: 'HTML Report', 
            reportTitles: 'Report'])                        
            }
        }
     }
```
    
  * **ARCHIVE** - Archives in Jenkins the archive files (generated during Assemble).
  
    - Updated the working directory to this stage.
    - Updated the directory of archiveArtifacts.
  
```
     stage('Archive') {
       steps {
          dir("CA2_Part2/demo"){
          echo 'Archiving...'
          archiveArtifacts 'build/libs/*'
          }
       }
      }
```
  
  * **PUBLISH IMAGE** - Generate a docker image with Tomcat and the war file and publish it in the Docker Hub.
  
    - New Dockerfile created in the project's folder, at the same level as the Jenkinsfile.
        - This Dockerfile was very similar to the one used the build the web container on Class Assignment 4. However, this dockerfile only needs to:
            - **1st** configure the files necessary to generate a linux image.
            - **2nd** make a copy of the existing war file to the correct location.
  
            ![](Images/Dockerfile.png)

    
    
     - New stage added with a step to:
         - Build a Docker Image, at the end of the pipeline.
         - Push the Image to Dockerhub. 
           
```
 stage ('Docker Image') {
     steps {
        dir("CA2_Part2/demo"){
        echo 'Building Docker image...'
        script {
        def imageTest = docker.build("elisabetevale/springbootappdevops:${env.BUILD_ID}", "--no-cache .")
        docker.withRegistry('https://registry.hub.docker.com', 'elisabetevale-dockerhub') {
        imageTest.push()
        }
        }
        }
     }
 }
```
  
#
   
**ADD JENKINSFILE TO REPOSITORY**
  
#
 
[Jenkinsfile](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part2/demo/Jenkinsfile)
  
```
 pipeline {
     agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://ElisabetedoVale@bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760.git'
            }
        }
        stage('Assemble') {
            steps {
               dir("CA2_Part2/demo"){
                   echo 'Assembling...'
                   bat 'gradlew clean build -x test'
               }
            }
        }
        stage('Test') {
             steps {
                dir("CA2_Part2/demo"){
                    echo 'Running tests...'
                    bat 'gradlew test'
                 }
              }
        }
        stage('Javadoc') {
            steps {
                dir("CA2_Part2/demo"){
                echo 'Generating Javadoc...'
                bat 'gradlew javadoc'
                publishHTML([allowMissing: false,
                alwaysLinkToLastBuild: true, 
                keepAll: true, 
                reportDir: '', 
                reportFiles: 'reportindex.html', 
                reportName: 'HTML Report', 
                reportTitles: 'Report'])                        
                }
            }
        }
        stage('Archive') {
            steps {
                dir("CA2_Part2/demo"){
                echo 'Archiving...'
                archiveArtifacts 'build/libs/*'
                }
            }
        }
        stage ('Docker Image') {
            steps {
                dir("CA2_Part2/demo"){
				echo 'Building Docker image...'
                script {
                        def imageTest = docker.build("elisabetevale/springbootappdevops:${env.BUILD_ID}", "--no-cache .")
                        docker.withRegistry('https://registry.hub.docker.com', 'elisabetevale-dockerhub') {
                        imageTest.push()
                        }
                }
                }
            }
        }
    } /* Stages*/

   post {
      always {
        junit 'CA2_Part2/demo/build/test-results/test/*.xml'
      }
   }

 } /* pipeline */

```
  
#
 
The pipeline have all the stages that we need so we must add them to remote repository executing:
 
 ```
 $ git add Jenkinsfile
 ```
 
 ```
 $ git commit -m "ref #47: ADD Jenkinsfile"
 ```
 
 ```
 $ git push
 ```

#

**ADD JENKINS PLUGINS**

The Jenkinsfile is already on the remote repository but before build it we need to add the following plugins:

  1. Java Doc 
  2. HTML Plugin
  
#
  
**BUILD THE PIPELINE**
  
On Jenkins page (http://localhost:9090) we build the pipeline and after that we can see:
  
#
  
  * Through the **Stage View**, if all the stages were successfully executed and how long it took to execute each one.
# 
   ![](Images/Jenkins_successfully_build.png)
  
#
  
  * At the **build history** if we click on the pretended build we can check the **Workspace** and be sure that if the build as done as expected.
#  
   ![](Images/Jenkins_Workspace.png)
  
#
  
  * We can also verify the **Test Result**.
#
   ![](Images/Jenkins_Test_Result.png)
  
#
  
  * To check if **Javadoc** it was generated correctly we must open the document 'HTML Report' (as we chose to call it) or download a ZIP file containing that same information.
     - Opening the Javadoc folder we must see the html reports:  
     - Opening some reports we can see the the documentation of that class:
#     
   ![](Images/Javadoc_HTMLReport.png)
  
#
          
   * Regarding to the **Docker Image** we must verify on **Docker Hub** that this was created.
#  
   ![](Images/DockerHub_image38.png)
  
#
      
   * We can also **verify if the image corresponds to the expected** running on PowerShell:
    
      - List all of our docker images.
      
         ```
         $ docker images
         ```
      
      - Run a container with that image.
      
         ```
         $ docker run -p 8080:8080 -d 22bb71b0071a
         ```      

      - Go to this container
      
         ```
         $ docker ps -a
         ```
         
      - Open the container console to check the war file
      
         ```
         $ docker exec -it 550dcc4f368f /bin/bash
         ```
               
 ![](Images/containerConsole.png)
       
  
## 2 | End of assignment part Tag
  
  - Create a new tag on master "CA5-part2"
  
This part of assignment is completed and so a tag should be created to mark the end of it.
To create the Tag locally, we execute:
  
 ```
 $ git tag -a CA5-part2 -m "CA5-part2 exercise it's complete"
 ```
  
  - Add the new tag to origin
The new tag has been locally created and must sent to the remote repository using the command:
  
 ```
 $ git push origin CA5-part2.
 ```
  
This tag marks the end of the Part 2 of assignment.