# Hyper-V as an alternative to Virtual Box


### Class Assignment 3 - Part 2 - Alternative

This technical report was developed in context of *DevOps* discipline, belonging to the *2019-2020 SWitCH Program* from *I.S.E.P.*.
  
For this part of assignment we will use as initial solution the Vagrantfile from CA3_Part2 Assignment, and it should be adapted now to an hypervisor alternative to VirtualBox.
  
As Spring Boot Application we also use the project already adapted to this assignment.
  
The download of Vagrantfile initial Soluction and the Spring Boot Project can be done at https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2.
    
  
## PART 0 - Project information - Sources and Repository
  
The goal of the present assignment is to present an alternative technological solution for the virtualization tool (i.e., an hypervisor alternative to VirtualBox). 

  
## PART I - Hyper-V as alternative to VirtualBox

In the modern highly virtualized IT world, choosing a hypervisor that is the most suitable for your tasks is an important point.
  
Hypervisor is special software that allows you to run one or multiple virtual machines with their own operating systems (guest operating systems) on a physical computer, which is called a host machine.
  
There are multiple virtualization platforms provided by different vendors, and each of them presents attractive features.
  
Oracle VirtualBox and Microsoft Hyper-V are two different hypervisors and they represents the two types of it - type 1 and type 2.
Hyper-V is a type 1 hypervisor, and runs directly on a computer�s hardware.
When a physical computer (a host) starts, a Hyper-V hypervisor takes control from BIOS or UEFI.
Then, Hyper-V starts the management operating system, which can be Hyper-V Server, Windows, or Windows Server.
Hyper-V is always on if the host is powered on, while the VirtualBox can be started and closed by a user on demand.

VirtualBox is a type 2 hypervisor that is sometimes called a hosted hypervisor.
A type 2 hypervisor is an application that runs on the operating system (OS) and is already installed on a host.
When a physical computer starts, the operating system installed on the host loads and takes control.
A user starts the hypervisor application (VirtualBox in this case) and then starts the needed virtual machines. VM hosted processes are created.
    
## PART III - Alternative implementation
  
For this part of assignment we will use as initial solution the Vagrantfile from CA3_Part2 Assignment, and it should be adapted now to an hypervisor alternative to VirtualBox.
As Spring Boot Application we also use the project already adapted to this assignment.
The download of Vagrantfile initial Soluction and the Spring Boot Project can be done at https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part2.
  
#### Step 1 | Activate Hyper-V
  
First we nee to verify that the machine supports virtualization.
To that we can verify the Hyper-V requirements, execute on console:

 ```
 $ systeminfo
 ```

Doing the fact of our machine supports virtualization we must activate Hyper-V.
To do that, we must follow the tutorial https://pplware.sapo.pt/tutoriais/hyper-v-crie-uma-maquina-dentro-do-windows-10/.

#### Step 2 |  Verify that the machine supports virtualization 

After activate Hyper-V we need to install the Hyper-V Box.
The scrip to install the can be downloaded at https://github.com/MicrosoftDocs/Virtualization-Documentation/issues/915.
After download the file we must run the script *HYPER-V Package Installer.bat* and after we need to restart our computer.
  
#### Step 3 |  Create a new directory

To start to work on project, we need to create a new directory and move to that, executing:

```
$ mkdir CA3-Part2_Alternative
$ cd CA3-Part2_Alternative
```
  
#### Step 4 |  ADD Spring Boot Project

Through the windows environment we copy the initial Project to CA3-Part2_Alternative on our local repository.
To send all the files of project and the folder of assignment to our remote repository we execute:

```
$ git add --all
$ git commit -m "colse #41: Initial Commit - Alternative_Ca3_part2"
$ git push
```

  
#### Step 5 | ADD Vagrantfile
  
Through the windows environment we copy the previous Vagrantfile to CA3-Part2_Alternative on our local repository.
After we commit the initial version of this Vagrantfile executing:
  
```
$ git add Vagrantfile
$ git commit -m "close #42: ADD Vagrantfile"
$ git push
```

#### Step 6 | UPDATE Vagrantfile
  
Using NotePad++ program we made the changes to Vagrantfile supports Hyper-V as alternative to Virtual Box.
The changes that need to be made on Vagrantfile are:
  
  - 1st. Add Hyperv as default provider
  
     *ENV['VAGRANT_DEFAULT_PROVIDER'] = "hyperv"*
     
  - 2nd. Change the information about machine names on private network
  
     *config.vm.box = "generic/ubuntu1604"*
    
     *config.vm.provider "hyperv"*
    
  - 3th. Change the configurations specific to the database VM
   
     *db.vm.box = "generic/ubuntu1604"*
     
     *# We set more ram memmory for this VM
      db.vm.provider "hyperv" do |v|
      v.memory = 1024
      end*
     
  - 4th. Change the configurations specific to the webserver VM
  
     *config.vm.define "web" do |web|*
      *web.vm.box = "generic/ubuntu1604"*
      
     *web.vm.provider "hyperv" do |v|
       v.memory = 1024
       end*
       
            
  - 5th. Change the path of project
  
      *cd devops-19-20-a-1191760/CA3_Part2_Alternative/hyperv*

  
After changes we commit again the vagrant file to our remote repository:
  
```
$ git add Vagrantfile
$ git commit -m "ref #43: UPDATE Vagrantfile"
$ git push
```

#### Step 7 | Execute Vagrantfile

Executing the Windows Power Shell as Admin we first change the directory to the directory of our project and were it is the vagrant file:
  
 ```
 $ cd "C:\Users\Elisabete Vale\IdeaProjects\devops-19-20-a-1191760\CA3_Part2_Alternative"
 ```
  
Already on the right folder, we execute:
  
 ```
 $ vagrant box add generic/ubuntu1604 --provider hyperv
 ```
  
And now we can execute our Vagrantfile executing:
  
 ```
 $ vagrant up
 ```

#### Step 8 | UPDATE web VM IP

When executing vagrant up we will have the IP information of our web VM.
We should change the IP on the file *application.properties* to the IP of our new web VM.
To that we must execute:
  
```
$ sudo sed -i -e 's/192.168.33.11/192.168.33.13/g'/home/vagrant/devops-19-20-a-1191760/CA3_Part2_Alternative/hyperv/src/main/resources/application.properties
$ cd /home/vagrant/devops-19-20-a-1121016/CA3/Part2_Alternative/hyperv
$ sudo ./gradlew clean build
$ sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
```
  
In the example above, our IP to change in application.properties would be 192.168.33.11 and will be changed to the IP of our web VM, for example, 192.168.33.13.
  
## PART IV - Issues report at the alternative implementation
  
Unfortunately I was unable to implement my alternative.
On step 7, when I execute vagrant up, the console reports that it was not possible to run the VM because the hipervisor was not running, despite everything indicating otherwise.
The following image reports the problem encountered:
  
![](./Images/errorReport.PNG)

However, if the alternative had worked we would finish the task by testing our Spring Boot Application at:

 > To web VM 

http://localhost:8080/demo-0.0.1-SNAPSHOT/ **or** http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/
  
 > To db VM

Http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console **or** http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console
