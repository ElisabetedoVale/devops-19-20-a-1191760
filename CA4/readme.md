# Containers with Docker
  
   
### Class Assignment 4
  
  
This technical report was developed in context of *DevOps* discipline, belonging to the *2019-2020 SWitCH Program* from *I.S.E.P.*.
    
The goal of the present assignment is to use Docker to setup a containerized environment to execute your version of the gradle version of spring basic tutorial application.
  
   
## Something about docker
  
Docker was first released in March 2013 and eveloped by *Docker, Inc.*. It is a tool designed to make it easier to create, deploy, and run applications by using containers Linux.
The container technology is the core of using other technologies like Docker Compose.
  
Docker Compose allows configuring and starting multiple docker containers and each container isolates memory, filesystem and network resources.

Docker containers and virtual machines (VMs) are very similar, since both of them are packaged computing environments that combine various IT components and isolate them from the rest of the system. Their main differences are in terms of scale and portability.
  
Compared to VMs, containers are best when we need to: 
  
  * Maximize the number of apps running on a server
  * Deploy multiple instances of a single application
  * Have a lightweight system that quickly starts
  * Develop an application that runs on any underlying infrastructure

Compared to containers, VMs are best when we need to:

  * Manage a variety of operating systems
  * Manage multiple apps on a single server
  * Run an app that requires all the resources and functionalities of an OS
  * Ensure full isolation and security
  
## 1 | Download of docker initial solution

For this assignment we will use as initial solution a project example with docker compose and it should be adapted to our Spring Boot project.
  
First, we start to **download** of the **docker-compose-spring-tut-demo** project to our computer at: https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/.
  
After download of docker initial solution we can verify that we have on the initial project:

  * One **db** directory with a dockerfile
  
    That represents the service/container used to execute the H2 server database.
  
  * One **web** directory with a docker file
  
      That represents the service/container used to run Tomcat and the spring application.
  
  * One file **docker-compose.yml**
   
     Which contains the commands needed to set up a scenario that included running the previous two dockerfiles.  
    
    
The next step is to create a new directory to work, so we must execute:
  
 ```
 $ mkdir CA4
 ```
  
To change our path to the new folder we execute:
  
 ```
 $ cd CA4
 ```
  
Through the windows environment we copy the files of the previous downloaded docker initial solution project to CA4 directory.
We must **copy**:

  - the docker-compose file;
  - the db directory;
  - the web directory.
   
We can now list our current directory and check that the files were copied to our local repository, using:

 ``` 
 $ ls
 ```
   
To add them to our remote repository we must **commit** them, executing:

``` 
$ git add --all
$ git commit -m "ref #44: ADD docker-compose-initial-solution"
$ git push
```
    
## 2 | Adaptation of the docker to our Spring Boot project

**UPDATE OF DOCKERFILE**

Before building our dockerfile images we need to make some changes to adapt to our Spring Boot application.
On web dockerfile we will:
  
[web/dockerfile]
  
  * Change the path of the repository to our Spring Boot project
  
 ```
 RUN git clone https://ElisabetedoVale@bitbucket.org/ElisabetedoVale/ca3_part2.git
 ```
 
  * Update the work directory of gradle spring application
    
 ```
 WORKDIR /tmp/build/ca3_part2/gradle_Spring
 ```
 
  * Add executable permissions to gradlew file
    
 ```
 RUN chmod +x gradlew
 ```
 
  * Update the name of the our war file
    
 ```
 RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
 ```
  
All the others files, namely the docker-compose and the db file don't suffer any changes.
  
The changes must be done also on the remote repository, so we execute:

``` 
$ git add --all
$ git commit -m "ref #45: UPDATE docker-compose-initial-solution : adapt to my SpringBoot Application"
$ git push
```
  
**INSTALL DOCKER**

With all the configurations updated we can know install the docker application and build the image of our containers.
  
Despite having Windows 10 Pro on my computer, which would allow me to access Docker with the simple installation of *docker-desktop* and the activation of *Hyper-V*, I continued to get an error in the execution of this procedure.
  
Given this fact, and as an alternative, I choose to use the VirtualBox where I had previously installed a Linux VM.
  
At first, I started with the VM with Ubuntu minimal, and in a second moment I accessed it through Windows PowerShell, just for considering it more user friendly.

To start the ssh session we execute:

 ```
 shh elisabetevale@192.168.56.5
 ```

Already on VM we must install docker executing:
  
 ```
 sudo apt install docker
 ```

And to install docker-compose we execute:
  
 ```
 sudo apt install docker-compose
 ```

Then it is necessary to add a new user to the docker group, so that they require permissions, so we execute:
  
 ```
 sudo usermod -aG docker elisabetevale
 ```

And then we should make logout and made login again.
  
Once again on the VM we can certify if docker have been correctly installed executing: 
  
 ```
 ls
 ```

**BUILD CONTAINERS IMAGES**
  
Already with docker on the VM we must clone our remote repository to there with:
  
 ```
 git clone https://ElisabetedoVale@bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760.git
 ```
  
Now we must move to the docker-compose folder:
  
 ```
 cd devops-19-20-a-1191760/CA4
 ```
  
And its time to build the images of web and db containers, executing:
  
 ```
 docker-compose build
 ```
  
Lastly we can run the docker-compose file to start the web and db containers, executing:
  
 ```
 docker-compose up
 ```
  
Then we can see that if our Spring Boot application is running correctly.
To check the web application we opening on browser the local host of our VM (using VM IP):
  
  - http://192.168.56.5:8080/demo-0.0.1-SNAPSHOT/
  
And web application is running as the image shows:
  
![](./Images/SpringBoot_web_docker_container.PNG)
  
To check the db application we also open the H2 console using:

  - http://192.168.56.5:8080/demo-0.0.1-SNAPSHOT/h2-console

    For the connection string we must use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb  

And db application is running as we see:
  
![](./Images/SpringBoot_h2_docker_container.PNG)
  
## 3 | Publish the images (db and web) to Docker Hub
 
Web and db docker images are working as correctly, so we can publish them to Docker hub.
  
To publish the images we must follow the steps:
  
  * Step 1 - Sign up on Docker hub (or create a new account) at https://hub.docker.com/
    - Username: ---------
    - Password: --------
    
  * Step 2 - Create a repository on docker hub
    
    - Name: springbootappdevops
    - Description: Spring Boot Images - Devops - CA4 - V1.0
    - Visibility: Public
      
  * Step 3 - login at dockerhub on PowerShell
   
  ```docker login```
        
  * Step 4 - Tag the images
    
  ```docker elisabetevale/springbootappdevops:ca4_db_v1.0``` - db  
  ```docker elisabetevale/springbootappdevops:ca4_db_v1.0``` - web
        
  * Step 5 - Push the images to Docker hub 
     
  ```docker push elisabetevale/springbootappdevops:ca4_db``` - db  
  ```docker push elisabetevale/springbootappdevops:ca4_db``` - web
  
   
## 4 | Get a copy of the database file
  
To the db container get a copy of the database file by using exec to run a shell in the container and copying the database file to the volume we execute:
  
 ```
 docker-compose exec db /bin/bash
 ``` 
  
To check if what is being executed:
  
 ```
 ps -ax
 ```
  
To check if the files were created, we must execute:
  
 ```
 ls -al
 ```
  
To map files from the container to my computer we must create a volume. So, on the docker-compose file we execute:
  
 ```
 - ./data:/usr/src/data
 ```  
 
To copy db file to the data directory on my computer and never lose the last data of database we must execute:
  
 ```
 cp ./jpadb.mv.db /usr/src/data
 ```
  
This gives us a safeguard case, for example, happen to lose the container.
  
## 5 | Readme file
  
The assignment is complete and to document the work done we must use the readme file. 
To create the readme file we execute:
  
 ```
 $ echo README >> README.md
 ```
  
After describing all the processes of the assignment on the README file, and after create issue on *Bitbucket*, we must commit all the changes executing the following commands:
  
```
$ git add readme.md
$ git commit -m "close #46: ADD Readme File"
$ git push
```
  
  
## 6 | End of assignment Tag
  
  
  - Create a new tag on master "CA4"
  
The CA4 assignment is completed and so a tag should be created to mark the end of it.
  
To create the Tag locally, we execute:
  
 ```
 $ git tag -a CA4 -m "CA4 exercise it's complete"
 ```
  
  - Add the new tag to origin
The new tag has been locally created and must sent to the remote repository using the command:
  
 ```
 $ git push origin CA4.
 ```
  
This tag marks the end of assignment.
  
------------
------------
  
### KUBERNETES: an alternative to docker-compose

Docker Compose, it’s a framework that allows developers to define container-based applications in a single YAML file.
This definition includes the Docker images used, exposed ports, dependencies, networking, etc.
  
Kubernetes is, according to the official website, “an open-source system for automating deployment, scaling, and management of containerized applications”.
Using containerization technology, Kubernetes allows running containers across several compute nodes that can be bare-metal servers or VMs.

The following are some of the fundamental features of Kubernetes:

  - Load balancing

  - Configuration management

  - Automatic IP assignment

  - Container scheduling

  - Health checks and self healing

  - Storage management

  - Auto rollback and rollout

  - Auto scaling