## Class Assignment 1 (CA1) Technical Report - Version Control with Git 

This technical report was developed in context of **DEVOPS** discipline belonging to the **2019-2020 SWitCH Program** from **I.S.E.P.**.

### I. Project sources

The download of the basic project for this assignment was done from [devops-19-20-rep-template](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1).
The source code and files needed for the assignment has been saved on folders [CA1/tut-basic/](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA1/tut-basic/).

### II. Safeguard of project Initial Version

#### Step 0: Create a Tag with the initial version "v1.2.0"

Git has the ability to tag specific points in a repository’s history, as being important stages. 
The initial version of the project is naturally a important stage, so, this point must be tagged.
To tag the initial version of the project, we just need to make run on the console (Windows PowerShell) the following command line:

```console
$ git tag -a v1.2.0 -m "Initial Version"
```

The tag have been created. To update this on repository, we have to execute the command line:

```console
$ git push origin v1.2.0
```

### III. Develop new features in branches

#### Step 1: Create a branch "email-field"

A branch represents an independent line of development. Branches serve as an abstraction for the edit/stage/commit process before addiction to master branch.
Until merge, the same project can have different developments and the repository history remains unchanged.

To create a new branch and starting to work on it, we just need to execute the following command:

```console
$ git checkout -B email-field
```

#### Step 2: Add support for "email-field".

Already on local branch 'email-field' we must add support to this branch.
Through the software IntelliJ IDEA, have been created a new attribute that will composes the class Employee. The new attribute is 'email', a String type.
The email have to be part of Employee constructor's class as parameter and, consequently, bring changes in the Equals, ToString and HashCode methods.
The implementation of this new attribute also brought two new methods: getEmail() and setEmail().

[©Employee](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA1/tut-basic/src/main/java/com/greglturnquist/payroll/Employee.java)
```java
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private String email;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String description, String jobTitle, String email) {
        if (firstName == null || firstName.equals("")) {
            throw new NullPointerException("Employee must have First Name");
        }
        if (lastName == null || lastName.equals("")) {
            throw new NullPointerException("Employee must have Last Name");
        }
        if (description == null || description.equals("")) {
            throw new NullPointerException("Employee must have Description");
        }
        if (jobTitle == null || jobTitle.equals("")) {
            throw new NullPointerException("Employee must have JobTitle");
        }
        if (email == null || email.equals("")) {
            throw new NullPointerException("Employee must have E-Mail");
        } else {
            this.firstName = firstName;
            this.lastName = lastName;
            this.description = description;
            this.jobTitle = jobTitle;
            this.jobTitle = jobTitle;
            this.email = email;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(jobTitle, employee.jobTitle) &&
                Objects.equals(email, employee.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, description, jobTitle, email);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
```

The implementation of email on Employee constructor's class also implicate the same changes on DatabaseLoader:

[©DatabaseLoader](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA1/tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java)
```java
public class DatabaseLoader implements CommandLineRunner { // <2>

	private final EmployeeRepository repository;

	@Autowired // <3>
	public DatabaseLoader(EmployeeRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception { // <4>
		this.repository.save(new Employee("Frodo", "Baggins", "ring bearer",  "Bearer", "email@email"));
		this.repository.save(new Employee("Maria", "do Vale", "Geography Department",  "Teacher", "maria@gmail"));
	}
}
```

#### Step 3: Add unit tests for testing the creation of Employees and the validation of its attributes (no null/empty values).

After all changes on business code have been made, it's time to add unit tests for testing the creation of Employees and the validation of its attributes (not null or empty values).
Follows some examples of the unit tests that have been done:

[©EmployeeTest](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA1/tut-basic/src/test/java/com/greglturnquist/payroll/EmployeeTest.java)
```java
class EmployeeTest {
    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Happy case: all parameters are matching")
    public void newEmployeeCreatedAllParametersMatching(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "maria@gmail.com";

        //Act
        Employee employeeMaria = new Employee(firstName, lastName, description, jobTitle, email);

        //Assert
        assertEquals(firstName, employeeMaria.getFirstName());
        assertEquals(lastName, employeeMaria.getLastName());
        assertEquals(description, employeeMaria.getDescription());
        assertEquals(jobTitle, employeeMaria.getJobTitle());
        assertEquals(email, employeeMaria.getEmail());
    }

//(...)

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: E-Mail can be Empty")
    public void newEmployeeNotCreatedEMailEmpty(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee must have E-Mail", thrown.getMessage());
    }

    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: E-Mail can be Null")
    public void newEmployeeNotCreatedEMailNull(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";

        //Assert
        Throwable thrown = assertThrows(NullPointerException.class, () -> new Employee(firstName, lastName, description, jobTitle, null));
        assertEquals("Employee must have E-Mail", thrown.getMessage());
    }
}
```

#### Step 4: Debug the server and client parts of the solution

With all steps before have been done, must be checked if everything it's working correctly, for that should be executed the following command line:

```console
$ ./mvnw spring-boot:run
```

After run the application, we can consult the localhost (on http://localhost:8080) if the Employee are being created correctly.
The application is correctly working, as shows the image [basic-3](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA1/tut-basic/images/basic-3.png).

#### Step 5: Merged branch "email-field" with the master branch

Assuming that all changes to the project were made and fully tested we can commit the developed work.
To check which files are not updated on Git we must run:

```console
$ git status
```

After check the files with changes, we need to update them. To add this files on Git, we must execute:

```console
$ git add tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java
$ git add tut-basic/src/main/java/com/greglturnquist/payroll/Employee.java
$ git add tut-basic/src/test/java/com/greglturnquist/payroll/EmployeeTest.java
```
With all files added, the next step is commit them, executing:

```console
$ git commit -m "ref #4: Add new Branch 'email-field' and tests"
```

After commit, the branch are pushed to the remote repository through the command:

```console
$ git push -u origin email-field
```

Now it's time to merge the branch email-field to branch master, but first of all we must checkout to the master branch:

```console
$ git checkout master
```

And so we can merge the branches, executing the command line:

```console
$ git merge email-field
```

The merge it's done, so the can commit the changes executing:

```console
$ git commit -m "ref #5:Merge Branch 'email-field' to Master"
```

After commit, all changes will be pushed to the remote repository through the command:

```console
$ git push
```

#### Step 6: Create a new Tag on master, update minor version to "v1.3.0"

Taking into account the importance of this stage in the application and assuming that everything is correctly implemented, tested and approved we must create a new tag in on repository, executing:

```console
$ git tag -a v1.3.0 -m "Email field: Feature commited and tested"
```

The new tag have been created and must sent to the remote repository using the command:

```console
$ git push origin v1.3.0
```

### IV. Develop new branches for fixing bugs

#### Step 7: Create a new branch "fix-invalid-email"

With the purpose of implements the email attribute validation, a new local branch must be created and all changes should be done on it.
To create a new branch and start to working on it, we must run the following command:

```console
$ git checkout -b fix-invalid-email
```

The server should only accept Employees with a valid email (e.g., an email must have the "@" sign).
To implement these requirements an IllegalArgumentException was added to the constructor reinforcing that "E-mail must have '@'".

[©Employee](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA1/tut-basic/src/main/java/com/greglturnquist/payroll/Employee.java)

```
    public Employee(String firstName, String lastName, String description, String jobTitle, String email) {
        if (firstName == null || firstName.equals("")) {
            throw new NullPointerException("Employee must have First Name");
        }
        if (lastName == null || lastName.equals("")) {
            throw new NullPointerException("Employee must have Last Name");
        }
        if (description == null || description.equals("")) {
            throw new NullPointerException("Employee must have Description");
        }
        if (jobTitle == null || jobTitle.equals("")) {
            throw new NullPointerException("Employee must have JobTitle");
        }
        if (email == null || email.equals("")) {
            throw new NullPointerException("Employee must have E-Mail");
        }
        if (!email.contains("@")) {
            throw new IllegalArgumentException("E-mail must have '@'");
        } else {
            this.firstName = firstName;
            this.lastName = lastName;
            this.description = description;
            this.jobTitle = jobTitle;
            this.jobTitle = jobTitle;
            this.email = email;
        }
```

After this changes on business code, it is necessary test the new validation added to the Employees constructor class, through the unit test:

[©EmployeeTest](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA1/tut-basic/src/test/java/com/greglturnquist/payroll/EmployeeTest.java)
```
    @Test
    @DisplayName("Test creation of new Employee || Check employee parameters || Sad case: E-Mail don't have @")
    public void newEmployeeNotCreatedEMailDoNotHaveAtSymbol(){

        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        String email = "mariagmail.com";

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("E-mail must have '@'", thrown.getMessage());
    }
```

#### Step 8: Debug the server and client parts of the solution.

Executing the following command we can see if the application is validating the email parameter or not:

```console
curl -X POST localhost:8080/api/employees -d "{\"firstName\": \"Maria\", \"lastName\": \"do Vale\", \"description\": \"Geography Department\", \"jobTitle\": \"Teacher\", \"email\": \"mariagmail.com\"}" -H "Content-Type:application/json"
``` 

And the result of the previous command line was: 

```console
{
Invoke-WebRequest : Cannot bind parameter 'Headers'. Cannot convert the "Content-Type:application/json" value of type "System.String" to type "System.Collections.IDictionary".
At line:1 char:206
+ ... ", \"email\": \"mariagmail.com\"}" -H "Content-Type:application/json"
+                                           ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : InvalidArgument: (:) [Invoke-WebRequest], ParameterBindingException
    + FullyQualifiedErrorId : CannotConvertArgumentNoMessage,Microsoft.PowerShell.Commands.InvokeWebRequestCommand
}
``` 

The error shows that the application can't built the parameter 'email' because the parameter binding an exception.

#### Step 9: Merged branch "fix-invalid-email" with the master branch

Done and fully tested all changes on the application code we can commit them, starting by add to Git the files changed:

```console
$ git add -A
```

The changed files are already added and so we can save the changes committing them to the local branch, using the following command:

```console
$ git commit -m "ref #6: Add new Branch 'fix-invalid-email'"
```

Now we should push the local branch 'fix-invalid-email' to the remote repository, executing:

```console
$ git push -u origin fix-invalid-email
```

The branch 'fix-invalid-email' have been added to remote repository, and need to be merged so, we must checkout to the master branch:

```console
$ git checkout master
```

Already on master branch, the merge between 'fix-invalid-email' and the master branch can be do it, using the following command:

```console
$ git merge fix-invalid-email
```

To commit the merge done, we must execute:

```console
$ git commit -m "ref #5:Merge Branch 'fix-invalid-email' to Master"
```

After commit, all changes will be pushed to the remote repository through the command:

```console
$ git push -u origin fix-invalid-email
```

#### Step 10: Create a new Tag on master, update patches version to "v1.3.1"

The bug have been fixed, so we should update the patches of the application version, executing the command line:

```console
$ git tag -a v1.3.1 -m "The server only accept a valid e-mail: e-mail must have '@' sign"
```

The new tag have been locally created and now it must sent to the remote repository using the command:

```console
$ git push origin v1.3.1
```

### V. Update report and final version

#### Step 11: Update readme.md file

A README file is a text file that contains information for the user about the software program.
README files often contain instructions and additional help, and details about patches or updates.
In this particular project this README file contains a simple technical report of the assignment CA1.

After README file edition is completed, the file must be add to git, executing:
 
```console
$ git add readme.md
```

The file is already on git and so the commit can be done running the following command:

```console
$ git commit -m "Update of readme file according to CA1"
```

After commit, the changes will be sent to the remote repository executing:
```console
$ git push
```

#### Step 12: Create a new Tag on master "CA1", this tag mark the end of the assignment

The CA1 assignment is completed and so a tag should be created to mark the end of it.
To create the Tag locally, we execute:

```console
$ git tag -a CA1 -m "CA1 exercise it's complete"
```

The new tag have been created and must sent to the remote repository using the command:

```console
$ git push origin CA1
```