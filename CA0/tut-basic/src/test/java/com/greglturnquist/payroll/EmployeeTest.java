package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test
    @DisplayName("Test creation of new Employee || Happy case")
    public void newEmployeeCreation(){
        //Arrange
        String firstName = "Maria";
        String lastName = "do Vale";
        String description = "Geography Department";
        String jobTitle = "Teacher";
        //Act
        Employee employeeMaria = new Employee(firstName, lastName, description, jobTitle);
        //Assert
        assertEquals(firstName, employeeMaria.getFirstName());
    }
}