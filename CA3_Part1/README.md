# Practice VirtualBox and Virtual Machine with Ubuntu 

### Class Assignment 3 (CA3) - Part 1

This technical report was developed in context of *DevOps* discipline, belonging to the *2019-2020 SWitCH Program* from *I.S.E.P.*.


## Part 0 - Project information: sources and repository

The goal of the present assignment is to practice with VirtualBox using the same projects from the previous assignments, but now inside a Virtual Machine with Ubuntu.

The basic project for this assignment results from the individual repository of this class, whose clone can be done at: [devops-19-20-a-1191760](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760).
The technical documentation that describe the process has been saved on folder [CA3_Part1](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA3_Part1).


## Part I - Create the Virtual Machine (VM)

Virtualization is the process of running a virtual instance of a computer system in a layer abstracted from the actual hardware.
There are many reasons why virtualization is used in computing but, in sum this technique offers:

  - The ability to run applications, meant for a specific operating system, into a different operating system;
  
  - Way to segment a large system into many smaller parts. This fact allowing the server to be used more efficiently by several different users or applications with different needs.

The first step of this assignment is creating a new Virtual Machine using **Oracle VM Virtual Box**.
The download of the application can be done at https://www.virtualbox.org/ and its installation should be done according to the instructions.

Already with the Virtual Box on our person computer we can create the new Virtual Machine, considering the following points:
  
  - The software type of the VM must be Linux;
  
  - The version must be the same of our computer version (32 or 64 bits);
  
  - The Memory Size (RAM) of the VM must be 2024MB;
  
  - The Virtual Hard Disc size must be 10GB;
  
  - The Virtual Hard Disc type must be CDI (VirtualBox Disk Image);
  
  - The Storage on Physical Hard Disk must be 'Dynamically allocated".

The VM have been created but we also need configure the Network Adapters. We need to configure two of them:
* Network Adapter 1 as Nat;
* Network Adapter 2 as Host-only Adapter (vboxnet0).
After configuration we must certify that both are 'enable'.

  
After that, we need to install the operative system desired. We will use **Ubuntu 18.04** (Linux). We can download it at https://help.ubuntu.com/community/Installation/MinimalCD and install it on VM through 'VMSettings'»'Storage'.
Already with the Virtual Machine working correctly, the follow commands lines proposed presuppose its execution on the console *VM command line* except for those that will be highlighted as such. 



## Part II - Clone the individual repository to inside the VM

#### Step 1: Install the dependencies of the projects

The individual repository use dependencies that must be installed on the VM as admin(sudo), so we must execute: 
    
   * To install GIT:
   
 ```
 $ sudo apt install git
 ```

   * To install JDK:
   
 ```
 $ sudo apt install openjdk-8-jdk-headless
 ```

   * To install Maven:
   
 ```
 $ sudo apt install maven
 ```

   * To install Gradle:
   
 ```
 $ sudo apt install gradle
 ```

#### Step 2: Clone the individual repository

To clone the remote repository, we verify first it address at *Bitbucket* platform and next we start it download into the VM executing:

 ```
 $ git clone https://ElisabetedoVale@bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760.git
 ```

Now we must check if the repository has been totally cloned. We can list the files that compose the directory, typing:

 ```
 $ ls
 ```

We can also check, directory by directory, the files that compose it one.
To do that, we just need to change the directory and list it again.



## Part III and IV - Build and execute the projects from the previous assignments

#### Step 1: Build and execute the *Maven* project (Spring Boot Application - CA1 Assigment)

First of all we need to change to the right directory of this particular project (/CA1/tut-basic folder) executing:

 ```
 $ cd CA1/tut-basic
 ```

Already on the right directory, we can try build the Maven file and execute the Spring Boot application however we will verify that the file does not have executable permissions.
The permissions of files can be check executing:
 
 ```
 $ ls -l
 ```

To add executable permission to *mvnw* file we must execute:
 
 ```
 $ chmod +x mvnw
 ```

Now the maven project can be built, and the Spring Boot application can be run, executing:
 
 ```
 $ ./mvnw spring-boot:run
 ```

The Spring Boot is running, and now we must check the *VM localhost* at *http://192.168.56.5:8080/*.

![](Images/LocalHost_VM.PNG)
  
![](Images/SpringBootBuild_VM.PNG)


#### Step 2: Build and execute the *Gradle* project (gradle_basic_demo - CA2 Assigment)

To this step we also need first to change the directory to where is the Gradle project (/CA2_Part1/demo), executing:

 ```
 $ cd CA2_Part1/demo
 ```

Already on the correct directory, we should also verify the files permissions, typing:

 ```
 $ ls -l
 ```

And then we check that the Gradle file does not have executable permissions, we need to change that, so we execute:
 
 ```
 $ chmod +x gradlew
 ```

Now the *gradlew* file is executable, and we can build the project, typing:
 
 ```
 $ ./gradlew build
 ```

Doing the fact of our VM only have a Ubuntu server, and does not have Desktop, to test the gradle application we must execute the Server inside the VM and the Client into the host machine (*Detail explanation at Part V*).
So, to run the server in the VM, we must execute:

 ```
 $ ./gradlew runServer
 ```

The Server part is running inside the VM and now we will run the Client part, in the project that we have in our laptop, outside of VM.
However, before the execute the task runClient we need to change itself, changing the 'Localhost' to '192.168.56.5', which is precisely the Localhost of our VM.

[build.gradle File](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part1/)


```java
 task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on 192.168.56.5"
  
    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
 }
 
```

Now we can run the Client part, from our laptop project, executing the following command at *Windows Power Shell* (on the directory of gradle project):

 ```
 $ ./gradlew runClient
 ```

To test the chat, we run again the Client part but on another console of *Windows Power Shell*.

 ```
 $ ./gradlew runClient
 ```

And now we have two windows of the chat application open and the simple chat is working correctly as we can see at the following image:

![](Images/SimpleChatApplication_VM.PNG)



## Part V - Issues encountered when the projects tests on VM

When we try to test the Maven and Gradle applications, we face some obstacles because as previously mentioned, our VM has only an Ubuntu server, and does not has a Desktop.
For example, when we run the 'runClient' task, of the gradle project, the application should open a new window to text the messages to other users. However, this window is a graphical window, part of a graphical environment, and so, cannot be executed inside the VM because this part is missing.
In order to overcome this obstacle, we run the server part inside the virtual machine and the client part on our computers, that have with graphical environment. To the 'runClient' task be connected to the 'runServer' (that are running on VM) we must change the Localhost from the first to the IP of our VM.
This problem would not exist if was installed the Ubuntu desktop version (with graphical environment). On the other hand if Ubuntu desktop version had been installed the VM would be heavier.



## Part VI - Add readme file of this assignment

The assignment is complete and to document the work done we create a new folder in our repository, executing:

 ```
 $ mkdir CA3_Part1
 ```

The folder is created, and the next step is creating the readme file, executing:

 ```
 $ echo README >> README.md
 ```

After describing all the processes of the assignment on the README file, and after create issue on *Bitbucket*, we must commit all the changes executing the following commands:

 ```
 $ git status
 $ git add CA3-Part1
 $ git commit -m "close #23: Add CA3-Part1 Assignment (Readme File)"
 $ git push
 ```

## Part VII - Tag the end of assignment

#### Step 1: Create a new tag on master "CA3-part1"

The CA3 - Part1 assignment is completed and so a tag should be created to mark the end of it.
To create the Tag locally, we execute:

 ```
 $ git tag -a CA3-part1 -m "CA3-part1 exercise it's complete"
 ```

#### Step 2: Add the new tag to origin
The new tag has been locally created and must sent to the remote repository using the command:

 ```
 $ git push origin CA3-part1.
 ```

This tag marks the end of the part 1 of assignment.