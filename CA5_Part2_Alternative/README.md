
# Bitbucket Pipelines as an alternative to Jenkins at CI/CD
  
   
### Class Assignment 5 - Alternative
  
This technical report was developed in context of *DevOps* discipline, belonging to the *2019-2020 SWitCH Program* from *I.S.E.P.*.
    
The goal of the present assignment is to is to create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2).
   
#

**PREVIOUS REQUIREMENTS:** This part of assignment presuppose that part 1 and 2 are already done.
  

## Bitbucket Pipelines, what is it?
  
Atlassian’s Bitbucket Pipelines is a lightweight cloud CI/CD server that uses pre-configured Docker containers, allowing you to define your infrastructure as code.
  
Such as Jenkins, this CI tool allows us to automatically build, test and even deploy our code, based on a configuration file (bitbucket-pipelines.yml file) in the root of our repository.
  
This file is our build configuration, and using configuration-as-code, means it is versioned and always in sync with the rest of our code.

#
  
![](Images/bp.PNG)

#### Implementing...

## 1 | Bitbucket configurations
#

*Note that:* The base-repository for this assignment resulted from a clone of the Ca2-Part2 class assignment project.

#

The **main goal** is to **reproduce the stages** that already have been defined on the Jenkisfile **of CA5-Part2**.
  
*Required stages:*
  
  * **CHECKOUT** - To check out the code from the repository.
    
  * **ASSEMBLE** - To compiles and Produces the archive files with the application, but without running the tests.
    
  * **TEST** - Executes the Unit Tests and publish in Jenkins the Test results.
      
  * **JAVADOC** - Generates the Javadoc of the project and publish it in Jenkins.
    
  * **ARCHIVE** - Archives in Jenkins the archive files (generated during Assemble).

  * **PUBLISH IMAGE** - Generate a docker image with Tomcat and the war file and publish it in the Docker Hub.

#
   
**ENABLE PIPELINES AT BITBUCKET**
  
Firstly, if we want to use pipelines in Bitbucket we must enable that setting.
To do that we must go to:
  
**Repository settings** >> **Pipelines settings** >> **Enable Pipelines: (ON)**
                                                                   
#

**ADD DOCKER HUB CREDENTIALS**

As we pretend to publish a docker image at Docker Hub we must add that credentials at **Repository settings** >> **Repository variables**.
We will need to define our **Username** and our **Password** of Docker Hub.

![](Images/2.PNG)
  
## 2 | Pipeline stages 
  
#
  
**CREATE THE PIPELINE**

#

The configurations are done and now we can create our pipeline.
  
To create the pipeline, we can choose to create a bitbucket-pipeline file and commit them to our repository or, in a first stage, create it directly in bitbucket.
  
To create the pipeline directly in bitbucket we must:

 * **1st:** Follow to **Pipelines** (on the toolbar on the left).
 
 * **2nd:** Choose the language template: **Java (Gradle)**.
 
 * **3rd:** Define the **pipeline steps** at the **bitbucket-pipelines.yml Editable**.
 
 * **4th:** **Commit** file.

After commit, the **bitbucket-pipeline.yml file** will be generated at the root of the bitbucket repository.
  
#
 
[bitbucket-pipeline.yml](https://bitbucket.org/ElisabetedoVale/ca2_part2_alternative/src/master/bitbucket-pipelines.yml)
  
```
 
 image: openjdk:8
 
 pipelines:
   custom:                          
     master:
         - step:
             name: Assembling...
             script: 
             - cd demo
             - chmod +x gradlew
             - bash ./gradlew clean assemble
             - git add -A
             - git commit -m "war file build nr. $BITBUCKET_BUILD_NUMBER"
             - git push
 
         - step:
             name: Test...
             script:
             - cd demo
             - git pull
             - chmod +x gradlew
             - bash ./gradlew test
             - git add -A
             - git commit -m "test reports build nr. $BITBUCKET_BUILD_NUMBER"
             - git push
 
         - step:
             name: Generating the Javadoc documentation...
             script:
             - cd demo
             - git pull
             - chmod +x gradlew
             - bash ./gradlew javadoc
             - git add -A
             - git commit -m "javadoc build nr. $BITBUCKET_BUILD_NUMBER"
             - git push
 
         - step:
             name: Archive...
             script:
             - cd demo
             - git pull
             - git tag -a build_nr._$BITBUCKET_BUILD_NUMBER -m "Build nr. $BITBUCKET_BUILD_NUMBER"
             - git push origin build_nr._$BITBUCKET_BUILD_NUMBER
 
             artifacts:
             - demo/build/libs/*.war
 
         - step:
             name: Docker image...
             script:
             - cd demo
             - docker login -u $DockerHub_Username -p $DockerHub_Password
             - docker build -t elisabetevale/springbootappdevops:$BITBUCKET_BUILD_NUMBER .
             - docker push elisabetevale/springbootappdevops:$BITBUCKET_BUILD_NUMBER
 
 options:
   docker: true

```
  
#

**RUN THE PIPELINE**

#

With the bitbucket-pipeline.yml complete and committed we should **run** it at:
  
**Pipelines** >> **Run pipelines**.
  
As we are only working on master, we will choose our *Branch* as 'master' and the *Pipeline* as 'custom: master'.

  * With the pipeline built we can see if all the **steps** had been ran correctly.  
#
   ![](Images/4..PNG)
  
#
  
  * Browsing through the steps we can check what was done in each one, including if the **war file** is in the **Artifacts** as expected.
# 
   ![](Images/5.PNG)
  
#
  
  * We can also verify by the **commits history** that **war file**, **tests reports** and **Javadoc** have been added to repository.
#  
   ![](Images/6.PNG)
  
#
  
  * Checking the **build folder** we can also see added files, how is the example of **Javadoc files**.
#
   ![](Images/8.PNG)
  
#
  
  * Last but not last, regarding to the **Docker Image** we must verify on **Docker Hub** that this was created.
# 
   ![](Images/9.PNG)
  

  
## 3 | Final considerations

#

Bitbucket Pipelines shows to be a great tool to execute  the objectives of a team **quickly**, if they are not very complex and the team is not too big.
   
This tool is very **intuitive**, we can monitor all that is required clicking in a checkbox in your project settings to enable the feature.
  
However, Bitbucket Pipelines is **fairly restricted** in what it allows you to do.
  
Until that moment this tool **does not support to build Windows applications or advanced functionalities**, add shows that it is not so flexible as other tools of CI.
  
As opposed, Jenkins proves to be a more involved and completed platform into the CI/CD world. Although **Bitbucket Pipelines** is also capable to be a CI/CD solution, although that will a **good solution only for a smaller and less complex scale**.


#